package mk.finki.emt.phoneshop.repository.jpa;

public class CategoryDto {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
