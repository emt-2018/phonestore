package mk.finki.emt.phoneshop.repository.jpa;

public interface CategoryById {

    public Long getId();
}
