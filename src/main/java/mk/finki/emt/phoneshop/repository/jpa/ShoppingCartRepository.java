package mk.finki.emt.phoneshop.repository.jpa;

import mk.finki.emt.phoneshop.model.ShoppingCart;
import mk.finki.emt.phoneshop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart,Long> {
    List<ShoppingCart> findByUser(User user);
}
