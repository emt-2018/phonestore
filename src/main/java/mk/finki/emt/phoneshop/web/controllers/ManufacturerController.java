package mk.finki.emt.phoneshop.web.controllers;

import mk.finki.emt.phoneshop.config.Layout;
import mk.finki.emt.phoneshop.model.Category;
import mk.finki.emt.phoneshop.model.Manufacturer;
import mk.finki.emt.phoneshop.service.CategoryService;
import mk.finki.emt.phoneshop.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/manufacturer")
public class ManufacturerController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ManufacturerService manufacturerService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Layout("layout/master")
    @GetMapping("/add")
    public String addManufacturer(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("manufacturer", new Manufacturer());
        return "forms/manufacturer.Add";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Layout("layout/master")
    @PostMapping("/save")
    public String saveManufacturer(@ModelAttribute Manufacturer manufacturer, Model model) {
        model.addAttribute("categories", categoryService.findAll());
        manufacturerService.save(manufacturer);
        return "common/success";
    }
}
