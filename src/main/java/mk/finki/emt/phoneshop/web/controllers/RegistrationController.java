package mk.finki.emt.phoneshop.web.controllers;

import mk.finki.emt.phoneshop.config.Layout;
import mk.finki.emt.phoneshop.model.User;
import mk.finki.emt.phoneshop.service.UserRoleService;
import mk.finki.emt.phoneshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value="/register")
public class RegistrationController {


    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Layout("layout/master")
    @GetMapping
    public String register(Model model) {
        model.addAttribute("userRoles",userRoleService.findAll());
        model.addAttribute("user",new User());
        return "register";
    }

    @Layout("layout/master")
    @PostMapping
    public String registerPost(@ModelAttribute User user, Model model) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(user);
        return "index";
    }
}
