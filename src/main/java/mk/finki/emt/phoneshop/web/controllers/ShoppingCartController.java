package mk.finki.emt.phoneshop.web.controllers;

import mk.finki.emt.phoneshop.model.Device;
import mk.finki.emt.phoneshop.model.ShoppingCart;
import mk.finki.emt.phoneshop.model.User;
import mk.finki.emt.phoneshop.service.DeviceService;
import mk.finki.emt.phoneshop.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/shoppingcart")
public class ShoppingCartController {


    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(method = RequestMethod.GET)
    public String shoppingCart(Principal principal) {
        User user = (User)principal;
        List<ShoppingCart> result = shoppingCartService.findByUser(user);
        return "shopping.cart";
    }

    @RequestMapping(value="/add",method=RequestMethod.POST)
    public String addToCart(Long deviceId) {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Device> d = deviceService.findById(deviceId);
        ShoppingCart sc = new ShoppingCart();
        sc.setUser(user);
        sc.setDevice(d.orElse(null));
        shoppingCartService.save(sc);
        return "shopping.cart";
    }
}
