package mk.finki.emt.phoneshop.web.controllers;

import mk.finki.emt.phoneshop.config.Layout;
import mk.finki.emt.phoneshop.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/search")
public class SearchController  {

    @Autowired
    private DeviceService deviceService;

    @Layout("layout/master")
    @RequestMapping(method = RequestMethod.POST)
    public String search(@RequestParam("searchText") String searchText, Model model) {
        model.addAttribute("searchText",searchText);
        model.addAttribute("devices",deviceService.search(searchText));
        return "search.Results";
    }
}
