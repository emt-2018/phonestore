package mk.finki.emt.phoneshop.service.impl;

import mk.finki.emt.phoneshop.model.UserRole;
import mk.finki.emt.phoneshop.repository.jpa.UserRoleRepository;
import mk.finki.emt.phoneshop.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService{


    @Autowired
    private UserRoleRepository repo;

    @Override
    public List<UserRole> findAll() {
        return repo.findAll();
    }
}
