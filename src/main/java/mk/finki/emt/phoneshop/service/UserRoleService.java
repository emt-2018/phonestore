package mk.finki.emt.phoneshop.service;

import mk.finki.emt.phoneshop.model.UserRole;

import java.util.List;

public interface UserRoleService {

    public List<UserRole> findAll();
}
