package mk.finki.emt.phoneshop.service.impl;

import mk.finki.emt.phoneshop.model.ShoppingCart;
import mk.finki.emt.phoneshop.model.User;
import mk.finki.emt.phoneshop.repository.jpa.ShoppingCartRepository;
import mk.finki.emt.phoneshop.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService{

    @Autowired
    private ShoppingCartRepository repo;

    @Override
    public Double calculateTotalPrice(ShoppingCart shoppingCart) {
        Double price = shoppingCart.getDevice().getPrice();
        price += price * 0.1;
        return price;
    }

    @Override
    public List<ShoppingCart> findByUser(User user) {
        return repo.findByUser(user);
    }

    @Override
    public Optional<ShoppingCart> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public ShoppingCart findOne(Long id) {
        return null;
    }

    @Override
    public Collection<ShoppingCart> findAll() {
        return null;
    }

    @Override
    public ShoppingCart save(ShoppingCart entity) {
        return null;
    }
}
