package mk.finki.emt.phoneshop.service;

import mk.finki.emt.phoneshop.model.ShoppingCart;
import mk.finki.emt.phoneshop.model.User;

import java.util.List;

public interface ShoppingCartService extends BaseEntityService<ShoppingCart> {

    public Double calculateTotalPrice(ShoppingCart shoppingCart);

    List<ShoppingCart> findByUser(User user);
}
